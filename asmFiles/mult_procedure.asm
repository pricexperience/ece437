#
#	ECE 437
#	Multiply Procedure
#	By Michael Price
#

# Base address
org 	0x0000

# Initialize stack
ori		$29, $0, 0xFFFC

# Initialize stack
ori		$1, $0, 0x0002
ori		$2, $0, 0x0003
ori		$3, $0, 0x0002
push 	$1
push	$2
push	$3

# Jump to multiplication Procedure
jal		mult_proc
pop		$2
halt

# Begin multiplication procedure
mult_proc:
	
	ori		$4, $0, 0xFFFC
	pop		$2
	beq		$29, $4, end
	pop		$3
	push	$31
	push	$3
	push	$2
	jal 	mult
	pop		$2
	pop		$31
	push	$2
	j 		mult_proc

end:
	
	push	$2
	jr		$31

# Begin multiplication sub routine
mult:

	ori 	$4, $0, 0x0000		# Result
	pop 	$2 					# Multiplier
	pop 	$3					# Multiplicand

loop:

	andi	$5, $3, 1
	beq		$5, $0, clear
	addu	$4, $4, $2

clear:

	sll		$2, $2, 1
	srl		$3, $3, 1
	bne		$3, $0, loop
	push	$4
	jr		$31
