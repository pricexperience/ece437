#
#	ECE 437
#	Multiply
#	By Michael Price
#	

# Base address
org		0x0000

# Initialize stack
ori		$29, $0, 0xFFFC

# Initialize operation registers
ori 	$2, $0, 0x0003
ori 	$3, $0, 0x0002
push	$2
push	$3	

# Jump into multiplcation routine
jal		mult
pop		$2 			# View register
halt

# Begin multiplication sub routine
mult:

	ori 	$4, $0, 0x0000		# Result
	pop 	$2 					# Multiplier
	pop 	$3					# Multiplicand

loop:

	andi	$5, $3, 1
	beq		$5, $0, clear
	addu	$4, $4, $2

clear:

	sll		$2, $2, 1
	srl		$3, $3, 1
	bne		$3, $0, loop
	push	$4
	jr		$31



# Notes
# To multiply by 2 shift to the left
# Loop through bits and multiply accordingly
# If (multiplicand & 1) result += multiplier << shift
