#
#	ECE 437
#	Multiply Procedure
#	By Michael Price
#

# Base address
org 	0x0000

# Initialize stack
ori		$29, $0, 0xFFFC

# Initialize current date
ori		$10, $0, 2 			# Day
ori		$11, $0, 9 			# Month
ori 	$12, $0, 2015 		# Year

# Initialize result register
ori		$13, $0, 0 			# Result

addi 	$14, $11, -1
ori 	$15, $0, 30

push 	$14
push 	$15
jal 	mult
pop 	$16

addi 	$17, $22, -2000
ori 	$18, $0, 365

push 	$17
push 	$18
jal 	mult
pop 	$19

add 	$20, $19, $16
add 	$21, $10, $20

halt

# Begin multiplication sub routine
mult:

	ori 	$4, $0, 0x0000		# Result
	pop 	$2 					# Multiplier
	pop 	$3					# Multiplicand

loop:

	andi	$5, $3, 1
	beq		$5, $0, clear
	addu	$4, $4, $2

clear:

	sll		$2, $2, 1
	srl		$3, $3, 1
	bne		$3, $0, loop
	push	$4
	jr		$31


# Notes
# Days = CurrentDay + (30 * (CurrentMonth - 1)) + 365 * (CurrentYear -2000)
