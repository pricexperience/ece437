/*
  ECE437
  Michael Price

  Request Unit Testbench
*/


`include "request_unit_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module request_unit_tb;
  
  request_unit_if ruif();

  // variables for tests
  logic CLK = 0, nRST;
  parameter PERIOD = 10;
  parameter DELAY = (PERIOD * 2);

  // DUT
  request_unit DUT(CLK, nRST, ruif);

  // Clock
  always #(PERIOD / 2) CLK++;

  initial begin
    int testnum;

    // Power on device
    $display("Initializeing device");
    nRST = 1'b0;
    #(DELAY);
    nRST = 1'b1;
    #(DELAY);

    // Test 1: control mem read write
    testnum++;
    ruif.dWrite = 1'b1;
    ruif.ihit = 1'b1;
    ruif.dhit = 1'b0;
    #(DELAY);
    if (ruif.dWEN == 1'b1) $display("TEST %2d passed", testnum);
    else $error("TEST %2d FAILED: dWEN = %d", testnum, ruif.dWEN);

    #(DELAY);
    // Test 2: check dhit
    testnum++;
    ruif.dhit = 1'b1;
    #(DELAY);
    if (ruif.dWEN == 1'b0) $display("TEST %2d passed", testnum);
    else $error("TEST %2d FAILED: dWEN = %d", testnum, ruif.dWEN);

    #(DELAY);
    // Test 3: control asserts mem read
    testnum++;
    ruif.dRead = 1'b1;
    ruif.ihit = 1'b1;
    ruif.dhit = 1'b0;
    #(DELAY);
    if (ruif.dREN == 1'b1) $display("TEST %2d passed", testnum);
    else $error("TEST %2d FAILED: dREN = %d", testnum, ruif.dREN);

    #(DELAY);
    // Test 4: check dhit
    testnum++;
    ruif.dhit = 1'b1;
    #(DELAY);
    if (ruif.dREN == 1'b0) $display("TEST %2d passed", testnum);
    else $error("TEST %2d FAILED: dREN = %d", testnum, ruif.dREN);

    $finish;
    
  end

endmodule