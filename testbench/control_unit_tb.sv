/*
  ECE437
  Michael Price

  Control Unit Testbench
*/


`include "control_unit_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module control_unit_tb;
  
  control_unit_if cuif();

  // variables for tests
  logic CLK = 0, nRST;
  parameter PERIOD = 10;
  parameter DELAY = (PERIOD * 2);

  int testnum = 0;
  int parttestnum = 0;

  // DUT
  control_unit DUT(cuif);

  // Clock
  always #(PERIOD / 2) CLK++;

  initial begin

    // Test 1: Check RegDst flag
    $display("Testing RegDst Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.RegDst == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.funct = JR;
    #(DELAY);
    if (cuif.RegDst == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 2: Check RegWrite flag
    $display("Testing RegWrite Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.RegWrite == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.funct = JR;
    #(DELAY);
    if (cuif.RegWrite == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = J;
    #(DELAY);
    if (cuif.RegWrite == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = BEQ;
    #(DELAY);
    if (cuif.RegWrite == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = BNE;
    #(DELAY);
    if (cuif.RegWrite == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SW;
    #(DELAY);
    if (cuif.RegWrite == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 3: Check MemRead flag
    $display("Testing MemRead Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.MemRead == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = LW;
    #(DELAY);
    if (cuif.MemRead == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = LL;
    #(DELAY);
    if (cuif.MemRead == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 4: Check MemWrite flag
    $display("Testing MemWrite Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.MemWrite == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SW;
    #(DELAY);
    if (cuif.MemWrite == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SC;
    #(DELAY);
    if (cuif.MemWrite == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 5: Check MemToReg flag
    $display("Testing MemToReg Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.MemToReg == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 6: Check BranchEq flag
    $display("Testing BranchEq Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.BranchEq == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = BEQ;
    #(DELAY);
    if (cuif.BranchEq == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 7: Check BranchNotEq flag
    $display("Testing BranchNotEq Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.BranchNotEq == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = BNE;
    #(DELAY);
    if (cuif.BranchNotEq == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 8: Check Jump flag
    $display("Testing Jump Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.Jump == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = J;
    #(DELAY);
    if (cuif.Jump == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = JAL;
    #(DELAY);
    if (cuif.Jump == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 9: Check Jal flag
    $display("Testing Jal Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.Jal == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = JAL;
    #(DELAY);
    if (cuif.Jal == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 10: Check RegToPC flag
    $display("Testing RegToPC Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.RegToPC == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.funct = JR;
    #(DELAY);
    if (cuif.RegToPC == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 11: Check Lui flag
    $display("Testing Lui Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.Lui == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = LUI;
    #(DELAY);
    if (cuif.Lui == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 12: Check SignExtImm flag
    $display("Testing SignExtImm Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.SignExtImm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = ADDIU;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SLTI;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SLTIU;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = LW;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SW;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = LL;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = SC;
    #(DELAY);
    if (cuif.SignExtImm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 13: Check Shamt flag
    $display("Testing Shamt Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.Shamt == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = RTYPE;
    cuif.funct = SLL;
    #(DELAY);
    if (cuif.Shamt == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = RTYPE;
    cuif.funct = SRL;
    #(DELAY);
    if (cuif.Shamt == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 14: Check Imm flag
    $display("Testing Imm Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.Imm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = J;
    #(DELAY);
    if (cuif.Imm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = JAL;
    #(DELAY);
    if (cuif.Imm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = BEQ;
    #(DELAY);
    if (cuif.Imm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = BNE;
    #(DELAY);
    if (cuif.Imm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = HALT;
    #(DELAY);
    if (cuif.Imm == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = LL;
    #(DELAY);
    if (cuif.Imm == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 15: Check Halt flag
    $display("Testing Halt Flag");
    testnum++;
    parttestnum = 1;
    cuif.opcode = RTYPE;
    cuif.funct = AND;
    #(DELAY);
    if (cuif.Halt == 1'b0) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    parttestnum++;
    cuif.opcode = HALT;
    #(DELAY);
    if (cuif.Halt == 1'b1) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);

    $finish;

  end

endmodule