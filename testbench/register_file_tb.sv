/*
  Eric Villasenor
  evillase@gmail.com

  register file test bench
*/

// mapped needs this
`include "register_file_if.vh"
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module register_file_tb;

  import cpu_types_pkg::*;

  parameter PERIOD = 10;

  logic CLK = 0, nRST;

  // test vars
  int v1 = 1;
  int v2 = 4721;
  int v3 = 25119;

  // clock
  always #(PERIOD/2) CLK++;

  // interface
  register_file_if rfif ();
    
  // test program
  test PROG (
    .CLK(CLK),
    .nRST(nRST),
    .rdat1(rfif.rdat1),
    .rdat2(rfif.rdat2),
    .rsel1(rfif.rsel1),
    .rsel2(rfif.rsel2),
    .WEN(rfif.WEN),
    .wsel(rfif.wsel),
    .wdat(rfif.wdat)
  );

  // DUT
`ifndef MAPPED
  register_file DUT(CLK, nRST, rfif);
`else
  register_file DUT(
    .\rfif.rdat2 (rfif.rdat2),
    .\rfif.rdat1 (rfif.rdat1),
    .\rfif.wdat (rfif.wdat),
    .\rfif.rsel2 (rfif.rsel2),
    .\rfif.rsel1 (rfif.rsel1),
    .\rfif.wsel (rfif.wsel),
    .\rfif.WEN (rfif.WEN),
    .\nRST (nRST),
    .\CLK (CLK)
  );
`endif

endmodule

program test
  import cpu_types_pkg::*;
(
  input logic CLK,
  input word_t rdat1,
  input word_t rdat2,
  output logic nRST,
  output logic WEN,
  output word_t wdat,
  output regbits_t wsel,
  output regbits_t rsel1,
  output regbits_t rsel2
);

// Tests
// 1. Test asynchronous reset of register
// 2. Test writes to register 0
// 3. Verify writes and reads to register

  parameter PERIOD = 10;
  int i;

  initial begin

    WEN = 0;
    wdat = 10;
    wsel = 0;
    rsel1 = 0;
    rsel2 = 0;
    
    // Power on device
    $display("Initializing device");
    nRST = 0;
    #(PERIOD);
    nRST = 1;

    #(PERIOD);
    // Verify writes to register 0, should be 0
    WEN = 1;
    #(PERIOD);
    assert (rdat1 == 0) $display("TEST 1: PASS");
    else $display("TEST 1: FAIL");

    #(PERIOD);
    // Write test data to all registers
    for (i = 0; i < 32; i++) begin

      wdat = 2**i;
      wsel = i;
      $display("Writing %d to register %d",wdat, wsel);
      #(PERIOD);

    end

    #(PERIOD);
    // Verify written data
    for (i = 0; i < 32; i++) begin

      rsel1 = i;
      #(PERIOD / 2);
      if (i == 0) begin
        assert (rdat1 == 0) $display("TEST 2: PASS");
        else $display("TEST 2: FAIL (%d should be %d)", rdat1, 0);
      end else begin
        assert (rdat1 == (2**i)) $display("TEST 2: PASS");
        else $display("TEST 2: FAIL (%d should be %d)", rdat1, (2**i));
      end
      #(PERIOD);

    end

    #(PERIOD);
    // Reset device and check registers, should all be 0
    $display("Resetting device");
    nRST = 0;
    #(PERIOD);
    nRST = 1;
    #(PERIOD);

    for (i = 0; i < 32; i++) begin

      rsel2 = i;
      assert ((rdat2 == 0)) $display("TEST 3: PASS");
      else $display("TEST 3: FAIL");
      #(PERIOD);

    end

  end

endprogram
