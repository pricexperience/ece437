/* 
	ECE437
	Michael Price

	Hazard Unit Testbench
*/

`include "hazard_unit_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module hazard_unit_tb;

	hazard_unit_if huif();

	logic CLK = 0, nRST;
	parameter PERIOD = 10;

	int testnum = 0;

	hazard_unit DUT(huif);

	always #(PERIOD / 2) CLK++;

	initial begin

		$display("Starting tests...");
		// Initializing inputs
		huif.flush_ifid = 0;
		huif.flush_idex = 0;
		huif.halt = 0;
		huif.pc_change = 0;
		huif.idex_MemRead = 0;
		huif.mem_access = 0;
		huif.idex_rt = 5'd0;
		huif.ifid_rt = 5'd0;
		huif.ifid_rs = 5'd0;
		huif.ihit = 0;
		huif.dhit = 0;

		#(PERIOD * 4);
		// TEST 1: IDEX stall on Memory Hazard
		testnum++;
		huif.idex_MemRead = 1;
		huif.idex_rt = 5'd1;
		huif.ifid_rs = 5'd1;
		#(PERIOD * 2);
		assert (huif.idex_stall == 1 && huif.ifid_stall == 0) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		huif.idex_MemRead = 0;
		huif.idex_rt = 5'd0;
		huif.ifid_rs = 5'd0;
		#(PERIOD * 4);
		// TEST 2: IFID stall on stall
		testnum++;
		huif.dhit = 1;
		#(PERIOD * 2);
		assert (huif.idex_stall == 0 && huif.ifid_stall == 1) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		huif.dhit = 0;
		#(PERIOD * 4);
		// TEST 3: All registers stall on memory access
		testnum++;
		huif.mem_access = 1;
		#(PERIOD * 2);
		assert (huif.all_stall == 1) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		huif.mem_access = 0;
		#(PERIOD * 4);
		// TEST 4: No stall on instruction fetch
		testnum++;
		huif.ihit = 1;
		#(PERIOD * 2);
		assert (huif.no_stall == 1) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		$finish;
	end

endmodule