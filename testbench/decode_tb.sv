/*
  ECE437
  Michael Price

  Decode Testbench
*/


`include "decode_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module decode_tb;
  
  decode_if dif();

  // variables for tests
  logic CLK = 0, nRST;
  parameter PERIOD = 10;
  parameter DELAY = (PERIOD * 2);

  int testnum = 0;
  int parttestnum = 0;

  // DUT
  decode DUT(dif);

  // Clock
  always #(PERIOD / 2) CLK++;

  initial begin

    // Test 1: Verify rtype instruction
    $display("Testing rtype instruction");
    testnum++;
    parttestnum = 1;
    dif.instr = 32'b11111100000111110000011111000000;
    #(DELAY);
    if (dif.opcode == 6'b111111) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.rs == 5'b00000) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.rt == 5'b11111) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.rd == 5'b00000) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.shamt == 5'b11111) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.funct == 6'b000000) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 2: Verify itype instruction
    $display("Testing itype instruction");
    testnum++;
    parttestnum = 1;
    dif.instr = 32'b11111100000111110000000000000000;
    #(DELAY);
    if (dif.opcode == 6'b111111) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.rs == 5'b00000) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
    if (dif.rt == 5'b11111) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
     if (dif.imm == 16'b0000000000000000) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);




    // Test 3: Verify jtype instruction
    $display("Testing jtype instruction");
    testnum++;
    parttestnum = 1;
    dif.instr = 32'b11111100000000000000000000000000;
    #(DELAY);
    if (dif.opcode == 6'b111111) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);
    parttestnum++;
     if (dif.addr == 26'b00000000000000000000000000) $display("TEST %d (%d): PASS", testnum, parttestnum);
    else $display("TEST %d (%d): FAIL", testnum, parttestnum);


    $finish;

  end

endmodule