/* 
	ECE437
	Michael Price

	Forwarding Unit Testbench
*/

`include "forwarding_unit_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module forwarding_unit_tb;

	forwarding_unit_if fuif();

	logic CLK = 0, nRST;
	parameter PERIOD = 10;

	int testnum = 0;
	word_t testdat1 = 32'd83;
	word_t testdat2 = 32'd26;
	word_t testdat3 = 32'd55;
	word_t testdat4 = 32'd65;

	forwarding_unit DUT(fuif);

	always #(PERIOD / 2) CLK++;

	initial begin

		$display("Starting tests...");
		// Initializing inputs
		fuif.ifid_rs = 5'd0;
		fuif.ifid_rt = 5'd0;
		fuif.idex_wsel = 5'd0;
		fuif.exmem_wsel = 5'd0;
		fuif.idex_RegWrite = 0;
		fuif.exmem_RegWrite = 0;
		fuif.exmem_out_forwardA = 0;
		fuif.memwb_out_forwardA = 0;
		fuif.exmem_out_forwardB = 0;
		fuif.memwb_out_forwardB = 0;
		fuif.exmem_out_wdat = word_t'(0);
		fuif.memwb_out_wdat = word_t'(0);
		fuif.idex_out_rdat1 = word_t'(0);
		fuif.idex_out_rdat2 = word_t'(0);

		#(PERIOD * 4);
		// TEST 1: Forward ALU result to port A
		testnum++;
		fuif.exmem_out_wdat = testdat1;
		fuif.ifid_rs = 5'd1;
		fuif.idex_RegWrite = 1;
		fuif.idex_wsel = 5'd1;
		fuif.exmem_out_forwardA = 1;
		#(PERIOD * 2);
		assert (fuif.forwardA == testdat1) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		fuif.exmem_out_forwardA = 0;
		#(PERIOD * 4);
		// TEST 2: Forward ALU result to port B 
		testnum++;
		fuif.exmem_out_forwardB = 1;
		#(PERIOD * 2);
		assert (fuif.forwardB == testdat1) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end		

		fuif.exmem_out_wdat = word_t'(0);
		fuif.ifid_rs = 5'd0;
		fuif.idex_RegWrite = 0;
		fuif.idex_wsel = 5'd0;
		fuif.exmem_out_forwardB = 0;
		#(PERIOD * 4);
		// TEST 3: Forward Memory Access result to port A
		testnum++;
		fuif.memwb_out_wdat = testdat2;
		fuif.ifid_rs = 5'd1;
		fuif.exmem_RegWrite = 1;
		fuif.exmem_wsel = 5'd1;
		fuif.memwb_out_forwardA = 1;
		#(PERIOD * 2);
		assert (fuif.forwardA == testdat2) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		fuif.memwb_out_forwardA = 0;
		#(PERIOD * 4);
		// TEST 4: Forward Memory Access result to port B
		testnum++;
		fuif.memwb_out_forwardB = 1;
		assert (fuif.forwardB == testdat2) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		fuif.memwb_out_wdat = word_t'(0);
		fuif.ifid_rs = 5'd0;
		fuif.exmem_RegWrite = 0;
		fuif.exmem_wsel = 5'd0;
		fuif.memwb_out_forwardB = 0;
		#(PERIOD * 4);
		// TEST 5: No forwarding on A
		testnum++;
		fuif.idex_out_rdat1 = testdat3;
		#(PERIOD * 2);
		assert (fuif.forwardA == testdat3) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		#(PERIOD * 4);
		// TEST 6: No forwarding on B
		testnum++;
		fuif.idex_out_rdat2 = testdat4;
		#(PERIOD * 2);
		assert (fuif.forwardB == testdat4) begin
			$display("TEST %d: PASS", testnum);
		end else begin
			$display("TEST %d: FAIL", testnum);
		end

		$finish;
	end

endmodule