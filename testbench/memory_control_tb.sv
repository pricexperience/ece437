/*
  ECE 437
  By Michael Price
*/

`include "cache_control_if.vh"
`include "cpu_ram_if.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module memory_control_tb;

  cache_control_if ccif();
  cpu_ram_if ramif();

  logic CLK = 0, nRST;

  parameter PERIOD = 10;
  parameter DELAY = (PERIOD * 2);
  parameter TEST_ADDR = 20;

  // DUT
  memory_control DUT(CLK, nRST, ccif);
  ram RAM(CLK, nRST, ramif);

  // Clock
  always #(PERIOD / 2) CLK++;

  // Tie ram and memory control
  assign ramif.ramWEN = ccif.ramWEN;
  assign ramif.ramREN = ccif.ramREN;
  assign ramif.ramstore = ccif.ramstore;
  assign ramif.ramaddr = ccif.ramaddr;
  assign ccif.ramstate = ramif.ramstate;
  assign ccif.ramload = ramif.ramload;
  
  initial begin
    int testnum;

    // Power on device
    $display("Initializeing device");
    nRST = 1'b0;
    #(DELAY);
    nRST = 1'b1;
    #(DELAY);

    // TEST 1: Read and write data
    testnum++;
    $display("TEST %d", testnum);
    ccif.dREN = 2'b00;
    ccif.dWEN = 2'b01;
    ccif.iREN = 2'b01;

    // Write data to 20 address spaces
    for (int i = 0; i <= TEST_ADDR; i = i + 4) begin
      ccif.daddr = i;
      ccif.dstore = i * 16;
      #(DELAY * 2);
    end

    // Read data values and verify
    ccif.dREN = 2'b01;
    ccif.dWEN = 2'b00;
    ccif.iREN = 2'b00;
    ccif.dstore = 0;
    #(DELAY * 2);

    // Read and verify data written
    for (int i = 0; i <= TEST_ADDR; i = i + 4) begin
      ccif.daddr = i;
      #(DELAY);
      if (ccif.dload[0] == (i * 16))
      begin
        $display("PASS - Read/Write successful");
      end else begin
        $error("FAIL - Read/Write unsuccessful");
      end
    end

    // Output contents of RAM to file

    // TEST 2: read instruction from RAM
    testnum++;

    // Write test instruction to adress 12
    ccif.dREN = 2'b00;
    ccif.dWEN = 2'b01;
    ccif.iREN = 2'b00;
    ccif.daddr = 12;
    ccif.dstore = 32'hFFEEFFEE;
    #(DELAY * 4);

    // Read instruction and verify
    ccif.dWEN = 2'b00;
    ccif.iREN = 2'b01;
    ccif.iaddr = 12;
    #(DELAY * 4);
    if (ccif.dload[0] == 32'hFFEEFFEE)
        $display("TEST %d PASS", testnum);
    else $error("TEST %d FAIL", testnum);

    #(DELAY * 4);
    // TEST 3: Test wait signals for instr read
    testnum++;
    ccif.dREN = 2'b00;
    ccif.dWEN = 2'b00;
    ccif.iREN = 2'b01;
    #(DELAY);
    if (ccif.dwait[0] == 1 && ccif.iwait[0] == 0)
      $display("TEST %d PASS", testnum);
    else $error("TEST %d FAIL", testnum);


    #(DELAY * 4);
    // TEST 4: Test wait signals for data read
    testnum++;
    ccif.dREN = 2'b01;
    ccif.dWEN = 2'b00;
    ccif.iREN = 2'b00;
    #(PERIOD * 2);
    if (ccif.dwait[0] == 0 && ccif.iwait[0] == 1)
      $display("TEST %d PASS", testnum);
    else $error("TEST %d FAIL", testnum);

    #(DELAY * 4);
    // TEST 5: Test wait signals for data write
    testnum++;
    ccif.dREN = 2'b00;
    ccif.dWEN = 2'b01;
    ccif.iREN = 2'b00;
    #(DELAY); 
    if (ccif.dwait[0] == 0 && ccif.iwait[0] == 1)
      $display("TEST %d PASS", testnum);
    else $error("TEST %d FAIL", testnum);

    #(DELAY * 4);
    // TEST 6: Send data and instr at same time, make sure data takes priority

    // Write test data to address 17
    testnum++;
    ccif.dREN = 2'b00;
    ccif.dWEN = 2'b01;
    ccif.iREN = 2'b00;
    ccif.daddr = 17;
    ccif.dstore = 32'hAABBAABB;
    #(DELAY * 4);

    // Now set instr and data RE high, should read data
    ccif.dREN = 2'b01;
    ccif.dWEN = 2'b00;
    ccif.iREN = 2'b01;
    #(DELAY);
    if (ccif.dload[0] == 32'hAABBAABB && ccif.dwait[0] == 1'b0 && ccif.iwait[0] == 1'b1)
        $display("TEST %d PASS", testnum);
    else $error("TEST %d FAIL", testnum);
    
    dump_memory();
    $finish;
  end

  task automatic dump_memory();
    string filename = "memcpu.hex";
    int memfd;

    ccif.daddr = 0;
    ccif.dWEN = 0;
    ccif.dREN = 0;

    memfd = $fopen(filename,"w");
    if (memfd)
      $display("Starting memory dump.");
    else
      begin $display("Failed to open %s.",filename); $finish; end

    for (int unsigned i = 0; memfd && i < 16384; i++)
    begin
      int chksum = 0;
      bit [7:0][7:0] values;
      string ihex;

      ccif.daddr = i << 2;
      ccif.dREN = 1;
      repeat (4) @(posedge CLK);
      if (ccif.dload === 0)
        continue;
      values = {8'h04,16'(i),8'h00,ccif.dload};
      foreach (values[j])
        chksum += values[j];
      chksum = 16'h100 - chksum;
      ihex = $sformatf(":04%h00%h%h",16'(i),ccif.dload,8'(chksum));
      $fdisplay(memfd,"%s",ihex.toupper());
    end //for
    if (memfd)
    begin
      ccif.dREN = 0;
      $fdisplay(memfd,":00000001FF");
      $fclose(memfd);
      $display("Finished memory dump.");
    end
  endtask

endmodule