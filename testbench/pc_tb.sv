/*
  ECE437
  Michael Price

  Program Counter Testbench
*/


`include "pc_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module pc_tb;
  
  pc_if pcif();

  // variables for tests
  logic CLK = 0, nRST;
  parameter PERIOD = 10;
  parameter DELAY = (PERIOD * 2);

  int testnum = 0;
  int parttestnum = 0;

  // DUT
  pc DUT(pcif);

  // Clock
  always #(PERIOD / 2) CLK++;

  initial begin

    $finish;

  end

endmodule