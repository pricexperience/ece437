/*

	ECE 437
	ALU Testbench
	By Michael Price

*/

`include "alu_if.vh"
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

import cpu_types_pkg::*;

module alu_tb;

	alu_if aluif();

	// Test program
	test PROG (
		.portA(aluif.portA),
		.portB(aluif.portB),
		.op(aluif.op),
		.negative(aluif.negative),
		.overflow(aluif.overflow),
		.zero(aluif.zero),
		.portOut(aluif.portOut)
	);

	// DUT
	`ifndef MAPPED
		alu DUT (aluif);
	`else
		alu DUT (
			.\aluif.portA (aluif.portA),
			.\aluif.portB (aluif.portB),
			.\aluif.op (aluif.op),
			.\aluif.negative (aluif.negative),
			.\aluif.overflow (aluif.overflow),
			.\aluif.zero (aluif.zero),
			.\aluif.portOut (aluif.portOut)
		);
	`endif

endmodule

program test 
	import cpu_types_pkg::*;	// Why do I have to re import this?
(
	input word_t portOut,
	input logic negative,
	input logic overflow,
	input logic zero,
	output word_t portA,
	output word_t portB,
	output aluop_t op
);

// Tests
// 1. Test each operation
// 2. Test each flag

	parameter DELAY = 4;
	int expected;

	initial begin

		// Test 1: Logical shift left
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_SLL;
		expected = (portA << portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 1: PASS");
		else $display("TEST 1: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 2: Logical shift right
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_SRL;
		expected = (portA >> portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 2: PASS");
		else $display("TEST 2: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 3: Addition
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_ADD;
		expected = (portA + portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 3: PASS");
		else $display("TEST 3: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 4: Subtraction
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_SUB;
		expected = (portA - portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 4: PASS");
		else $display("TEST 4: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 5: And
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_AND;
		expected = (portA & portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 5: PASS");
		else $display("TEST 5: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 6: Or
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_OR;
		expected = (portA | portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 6: PASS");
		else $display("TEST 6: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 7: Xor
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_XOR;
		expected = (portA ^ portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 7: PASS");
		else $display("TEST 7: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 8: Nor
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_NOR;
		expected = ~(portA | portB);
		#(DELAY);
		assert (portOut == expected) $display("TEST 8: PASS");
		else $display("TEST 8: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 9: Set less than signed
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_SLT;
		expected = ($signed(portA) < $signed(portB));
		#(DELAY);
		assert (portOut == expected) $display("TEST 9: PASS");
		else $display("TEST 9: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 10: Set less than unsigned
		portA = 32'd10;
		portB = 32'd1;
		op = ALU_SLTU;
		expected = ($unsigned(portA) < $unsigned(portB));
		#(DELAY);
		assert (portOut == expected) $display("TEST 10: PASS");
		else $display("TEST 10: FAIL (Output %b, Expected %b)", portOut, expected);

		// Test 11: Negative flag
		portA = 32'd10;
		portB = 32'd11;
		op = ALU_SUB;
		expected = (portA - portB);
		#(DELAY);
		assert (negative == 1'b1) $display("TEST 11: PASS");
		else $display("TEST 10: FAIL (Negative flag %b) Port Out: %b", negative, portOut);

		// Test 12: Overflow flag ADD
		portA = 32'h7FFFFFFF;
		portB = 32'd1;
		op = ALU_ADD;
		expected = (portA + portB);
		#(DELAY);
		assert (overflow == 1'b1) $display("TEST 12: PASS");
		else $display("TEST 12: FAIL (Overflow flag %b) Port Out: %b", overflow, portOut);

		// Test 13: Overflow flag SUB
		portA = 32'd1;
		portB = -32'h7FFFFFFF;
		op = ALU_SUB;
		expected = (portA - portB);
		#(DELAY);
		assert (overflow == 1'b1) $display("TEST 13: PASS");
		else $display("TEST 13: FAIL (Overflow flag %b)", overflow);

		// Test 14: Zero flag
		portA = 32'd10;
		portB = 32'd10;
		op = ALU_SUB;
		expected = (portA - portB);
		#(DELAY);
		assert (zero == 1'b1) $display("TEST 14: PASS");
		else $display("TEST 14: FAIL (Zero flag %b)", zero);

	end

endprogram