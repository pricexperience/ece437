/*
	ECE 437
	By Michael Price
*/

// Includes
`include "pipeline_register_if.vh"
`include "cpu_types_pkg.vh"
`include "pipeline_types_pkg.vh"

module pipeline_register (
	input logic CLK, nRST,
	pipeline_register_if.pr prif
);

	// Types
	import cpu_types_pkg::*;
	import pipeline_types_pkg::*;

	always_ff @ (posedge CLK, negedge nRST) begin

		if (!nRST) begin

			prif.ifid_out <= ifid_t'(0);
			prif.idex_out <= idex_t'(0);
			prif.exmem_out <= exmem_t'(0);
			prif.memwb_out <= memwb_t'(0);

		end else begin 

			if (prif.ifid_stall) begin // IFID stall
				prif.ifid_out <= ifid_t'(0);

				if (prif.idex_flush) begin
					prif.idex_out <= idex_t'(0);
				end else begin
					prif.idex_out <= prif.idex_in;
				end

				prif.exmem_out <= prif.exmem_in;
				prif.memwb_out <= prif.memwb_in;
			end else if (prif.idex_stall) begin // IDEX stall
				//prif.ifid_out <= prif.ifid_in;
				prif.idex_out <= idex_t'(0);
				prif.exmem_out <= prif.exmem_in;
				prif.memwb_out <= prif.memwb_in;
			end else if (prif.all_stall) begin // All stall
				prif.ifid_out <= prif.ifid_out;
				prif.idex_out <= prif.idex_out;
				prif.exmem_out <= prif.exmem_out;
				prif.memwb_out <= prif.memwb_out;
			end else if(prif.no_stall) begin // No stall
				if (prif.ifid_flush) begin
					prif.ifid_out <= ifid_t'(0);
				end else begin
					prif.ifid_out <= prif.ifid_in;
				end

				if (prif.idex_flush) begin
					prif.idex_out <= ifid_t'(0);
				end else begin
					prif.idex_out <= prif.idex_in;
				end

				prif.exmem_out <= prif.exmem_in;
				prif.memwb_out <= prif.memwb_in;
			end

		end

	end

endmodule