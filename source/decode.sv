/*
  ECE 437
  By Michael Price

  Decode

  - Determine whether instruction is an rtype, itype, or jtype
  - Filter instruction parts 

  Inputs
    instr

  Outputs
    opcode
    rs
    rt
    rd
    shamt
    funct
    imm
    addr
*/

// Includes
`include "decode_if.vh"
`include "cpu_types_pkg.vh"

module decode (
  decode_if.d dif
);

  // Import types
  import cpu_types_pkg::*;

  //opcode_t op;

  always_comb begin

    dif.opcode = opcode_t'(dif.instr[31:26]);
    dif.rs = dif.instr[25:21];
    dif.rt = dif.instr[20:16];
    dif.rd = dif.instr[15:11];
    dif.shamt = dif.instr[10:6];
    dif.funct = funct_t'(dif.instr[5:0]);
    dif.imm = dif.instr[15:0];
    dif.addr = dif.instr[25:0];

    /*op = opcode_t'(dif.instr[31:26]);
    dif.opcode = op;

    if (op === RTYPE) begin
      dif.rs = dif.instr[25:21];
      dif.rt = dif.instr[20:16];
      dif.rd = dif.instr[15:11];
      dif.shamt = dif.instr[10:6];
      dif.funct = funct_t'(dif.instr[5:0]);
    end else if (op === J || op === JAL) begin
      dif.addr = dif.instr[25:0];
    end else begin
      dif.rs = dif.instr[25:21];
      dif.rt = dif.instr[20:16];
      dif.imm = dif.instr[15:0];
    end*/

  end

endmodule