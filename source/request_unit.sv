/*

	ECE437
	By Michael Price

	Request Unit
*/

// Includes
`include "request_unit_if.vh"
`include "cpu_types_pkg.vh"

module request_unit (
	input logic CLK, nRST,
	request_unit_if.ru ruif
);

	// Import types
	import cpu_types_pkg::*;

	always_ff @ (posedge CLK, negedge nRST) begin

		if (!nRST) begin
			ruif.dREN <= 0;
			ruif.dWEN <= 0;
			ruif.iREN <= 1;
		end else if (ruif.halt) begin
			ruif.dREN <= 0;
			ruif.dWEN <= 0;
			ruif.iREN <= 0;
		end else if (ruif.ihit && ~ruif.dhit) begin
			ruif.dREN <= ruif.dRead;
			ruif.dWEN <= ruif.dWrite;
			ruif.iREN <= 1;
		end else if (ruif.dhit) begin
			ruif.dREN <= 0;
			ruif.dWEN <= 0;
			ruif.iREN <= 1;
		end

	end

endmodule