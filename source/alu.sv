/*

	ECE 437
	ALU Component
	By Michael Price

*/

`include "cpu_types_pkg.vh"
`include "alu_if.vh"

import cpu_types_pkg::*;	// Can this be outside of module? It was working fine on register_file

module alu (
	// Purely combinational
	alu_if.alu aluif
);

	always_comb begin
		// Default values
		aluif.portOut = 0;
		aluif.negative = 0;
		aluif.overflow = 0;
		aluif.zero = 0;

		casez (aluif.op)

			ALU_SLL: begin
				aluif.portOut = (aluif.portA << aluif.portB);
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_SRL: begin
				aluif.portOut = (aluif.portA >> aluif.portB);
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_ADD: begin
				aluif.portOut = ($signed(aluif.portA) + $signed(aluif.portB));
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
				
				aluif.overflow = (aluif.portA[WORD_W - 1] == aluif.portB[WORD_W-1]) && (aluif.portOut[WORD_W-1] != aluif.portA[WORD_W - 1]);
			end

			ALU_SUB: begin
				aluif.portOut = ($signed(aluif.portA) - $signed(aluif.portB));
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
				
				aluif.overflow = (aluif.portA[WORD_W - 1] != aluif.portB[WORD_W-1]) && (aluif.portOut[WORD_W-1] != aluif.portA[WORD_W - 1]);
			end

			ALU_AND: begin
				aluif.portOut = (aluif.portA & aluif.portB);
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_OR: begin
				aluif.portOut = (aluif.portA | aluif.portB);
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_XOR: begin
				aluif.portOut = (aluif.portA ^ aluif.portB);
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_NOR: begin
				aluif.portOut = ~(aluif.portA | aluif.portB);
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_SLT: begin
				aluif.portOut = ($signed(aluif.portA) < $signed(aluif.portB));
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

			ALU_SLTU: begin
				aluif.portOut = ($unsigned(aluif.portA) < $unsigned(aluif.portB));
				aluif.negative = (aluif.portOut[WORD_W - 1] == 1);
				aluif.zero = (aluif.portOut == 0);
			end

		endcase

	end

endmodule

// Notes
//aluif.overflow = ~(aluif.portA[WORD_W-1] ^ aluif.portB[WORD_W-1]) & (aluif.portA[WORD_W-1] ^ aluif.portOut[WORD_W-1]);