/*
	ECE437
	By Michael Price

	Program Counter
*/

// Includes
`include "pc_if.vh"
`include "cpu_types_pkg.vh"

module pc (
	input logic CLK, nRST,
	pc_if.pc pcif
);

	// Import Types
	import cpu_types_pkg::*;

	// PC Init
  	parameter PC_INIT = 0;

	always_ff @ (posedge CLK, negedge nRST) begin

		if (!nRST) begin
			pcif.current_pc <= PC_INIT; // Initialize PC
		end else if (pcif.WEN) begin
			pcif.current_pc <= pcif.new_pc;
		end

	end

	// Next PC
	always_comb begin
		pcif.next_pc = pcif.current_pc + WBYTES;
	end

endmodule