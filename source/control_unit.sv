/*
  ECE 437
  By Michael Price

  Control Unit Interface

  Inputs
	opcode
	funct

  Outputs (Control Signals)
  	RegDst
	RegWrite
	ALUSrc
	ALUOp
	MemRead
	MemWrite
	MemToReg

	Branch - use imm
	Jump - use addr
	*JR
	*JAL
	*BNE
*/

// Includes
`include "control_unit_if.vh"
`include "cpu_types_pkg.vh"

module control_unit (
	control_unit_if.cu cuif
);

	// Types
	import cpu_types_pkg::*;

	// ALU opcode logic
	always_comb begin

		cuif.ALUOp = ALU_ADD; // Default case

		if (cuif.opcode === RTYPE) begin
			casez (cuif.funct) 
				SLL: cuif.ALUOp = ALU_SLL;
				SRL: cuif.ALUOp = ALU_SRL;
				ADD: cuif.ALUOp = ALU_ADD;
				ADDU: cuif.ALUOp = ALU_ADD;
				SUB: cuif.ALUOp = ALU_SUB;
				SUBU: cuif.ALUOp = ALU_SUB;
				AND: cuif.ALUOp = ALU_AND;
				OR: cuif.ALUOp = ALU_OR;
				XOR: cuif.ALUOp = ALU_XOR;
				NOR: cuif.ALUOp = ALU_NOR;
				SLT: cuif.ALUOp = ALU_SLT;
				SLTU: cuif.ALUOp = ALU_SLTU;
			endcase
		end else begin
			casez (cuif.opcode)
				ADDIU: cuif.ALUOp = ALU_ADD;
				ANDI: cuif.ALUOp = ALU_AND;
				ORI: cuif.ALUOp = ALU_OR;
				SLTI: cuif.ALUOp = ALU_SLT;
				SLTIU: cuif.ALUOp = ALU_SLTU;
				XORI: cuif.ALUOp = ALU_XOR;
				BEQ: cuif.ALUOp = ALU_SUB;
				BNE: cuif.ALUOp = ALU_SUB;
				SW: cuif.ALUOp = ALU_ADD;
				LW: cuif.ALUOp = ALU_ADD;
			endcase
		end

	end


	always_comb begin

		// Defaults
		cuif.RegDst = 0;
		cuif.RegWrite = 1;
		cuif.MemRead = 0;
		cuif.MemWrite = 0;
		cuif.BranchEq = 0;
		cuif.BranchNotEq = 0;
		cuif.Jump = 0;
		cuif.Jal = 0;
		cuif.RegToPC = 0;
		cuif.Lui = 0;
		cuif.SignExtImm = 0;
		cuif.Shamt = 0;
		cuif.Imm = 1;
		cuif.Halt = 0;

		casez (cuif.opcode)

			// Rtype
			RTYPE: begin
				cuif.Imm = 0;
				cuif.RegDst = 1;
				cuif.RegWrite = 1;
				cuif.RegToPC = 0;
				cuif.Shamt = 0;

				if (cuif.funct === JR) begin 
					cuif.RegWrite = 0;
					cuif.RegToPC = 1;
					cuif.RegDst = 0;
				end /*else begin
					cuif.RegWrite = 1;
					cuif.RegToPC = 0;
				end*/

				if (cuif.funct === SLL || cuif.funct === SRL) begin
					cuif.Shamt = 1;
				end /*else begin
					cuif.Shamt = 0;
				end*/

			end

			// Jtype
			J: begin
				cuif.Jump = 1;
				cuif.RegWrite = 0;
				cuif.Imm = 0;
			end
			JAL: begin
				cuif.Jump = 1;
				cuif.Imm = 0;
				cuif.Jal = 1;
			end

			// Itype
			BEQ: begin
				cuif.BranchEq = 1;
				cuif.RegWrite = 0;
				cuif.Imm = 0;
			end
			BNE: begin
				cuif.BranchNotEq = 1;
				cuif.RegWrite = 0;
				cuif.Imm = 0;
			end
			ADDI: begin

			end
			ADDIU: begin
				cuif.SignExtImm = 1;
			end
			SLTI: begin
				cuif.SignExtImm = 1;
			end
			SLTIU: begin
				cuif.SignExtImm = 1;
			end
			ANDI: begin
			end
			ORI: begin
			end
			XORI: begin
			end
			LUI: begin
				cuif.Lui = 1;
			end
			LW: begin
				cuif.SignExtImm = 1;
				cuif.MemRead = 1;
			end
			LBU: begin

			end
			LHU: begin

			end
			SB: begin

			end
			SH: begin

			end
			SW: begin
				cuif.RegWrite = 0;
				cuif.SignExtImm = 1;
				cuif.MemWrite = 1;
			end
			LL: begin
				cuif.SignExtImm = 1;
				cuif.MemRead = 1;
			end
			SC: begin
				cuif.SignExtImm = 1;
				cuif.MemWrite = 1;
			end
			HALT: begin
				cuif.RegDst = 0;
				cuif.RegWrite = 0;
				cuif.MemRead = 0;
				cuif.MemWrite = 0;
				cuif.BranchEq = 0;
				cuif.BranchNotEq = 0;
				cuif.Jump = 0;
				cuif.Jal = 0;
				cuif.RegToPC = 0;
				cuif.Lui = 0;
				cuif.SignExtImm = 0;
				cuif.Shamt = 0;
				cuif.Imm = 0;
				cuif.Halt = 1;
			end

		endcase

	end

/*
	always_comb begin

		// Defaults
		cuif.RegDst = 0;
		cuif.RegWrite = 1;
		cuif.MemRead = 0;
		cuif.MemWrite = 0;
		//cuif.MemToReg = 0;
		cuif.BranchEq = 0;
		cuif.BranchNotEq = 0;
		cuif.Jump = 0;
		cuif.Jal = 0;
		cuif.RegToPC = 0;
		cuif.Lui = 0;
		cuif.SignExtImm = 0;
		cuif.Shamt = 1;
		cuif.Imm = 1;
		cuif.Halt = 0;

		// Default ALU Operation
		cuif.ALUOp = ALU_ADD;

		casez (cuif.opcode)

			// Rtype
			RTYPE: begin
				cuif.Imm = 0;
				cuif.RegDst = 1;

				if (cuif.funct === JR) begin 
					cuif.RegWrite = 0;
					cuif.RegToPC = 1;
					cuif.RegDst = 0;
				end else begin
					cuif.RegWrite = 1;
					cuif.RegToPC = 0;
					cuif.RegDst = 1;
				end

				if (cuif.funct === SLL || cuif.funct === SRL) begin
					cuif.Shamt = 1;
				end else begin
					cuif.Shamt = 0;
				end

				cuif.ALUOp = alu_funct;

			end

			// Jtype
			J: begin
				cuif.Jump = 1;
				cuif.RegWrite = 0;
				cuif.Imm = 0;
			end
			JAL: begin
				cuif.Jump = 1;
				cuif.Imm = 0;
				cuif.Jal = 1;
			end

			// Itype
			BEQ: begin
				cuif.BranchEq = 1;
				cuif.RegWrite = 0;
				cuif.Imm = 0;
				cuif.ALUOp = ALU_SUB;
			end
			BNE: begin
				cuif.BranchNotEq = 1;
				cuif.RegWrite = 0;
				cuif.Imm = 0;
				cuif.ALUOp = ALU_SUB;
			end
			ADDI: begin

			end
			ADDIU: begin
				cuif.SignExtImm = 1;
			end
			SLTI: begin
				cuif.SignExtImm = 1;
				cuif.ALUOp = ALU_SLT;
			end
			SLTIU: begin
				cuif.SignExtImm = 1;
				cuif.ALUOp = ALU_SLTU;
			end
			ANDI: begin
				cuif.ALUOp = ALU_AND;
			end
			ORI: begin
				cuif.ALUOp = ALU_OR;
			end
			XORI: begin
				cuif.ALUOp = ALU_XOR;
			end
			LUI: begin
				cuif.Lui = 1;
			end
			LW: begin
				cuif.SignExtImm = 1;
				cuif.MemRead = 1;
			end
			LBU: begin

			end
			LHU: begin

			end
			SB: begin

			end
			SH: begin

			end
			SW: begin
				cuif.RegWrite = 0;
				cuif.SignExtImm = 1;
				cuif.MemWrite = 1;
			end
			LL: begin
				cuif.SignExtImm = 1;
				cuif.MemRead = 1;
			end
			SC: begin
				cuif.SignExtImm = 1;
				cuif.MemWrite = 1;
			end
			HALT: begin
				cuif.Halt = 1;
				cuif.Imm = 0;
			end

		endcase

	end




	always_comb begin
		alu_funct = ALU_ADD; // Default

		casez (cuif.funct)

			SLL: alu_funct = ALU_SLL;
			SRL: alu_funct= ALU_SRL;
			SUB: alu_funct = ALU_SUB;
			SUBU: alu_funct = ALU_SUB;
			AND: alu_funct = ALU_AND;
			OR: alu_funct = ALU_OR;
			XOR: alu_funct = ALU_XOR;
			NOR: alu_funct = ALU_NOR;
			SLT: alu_funct = ALU_SLT;
			SLTU: alu_funct = ALU_SLTU;

		endcase
	end
*/
endmodule
