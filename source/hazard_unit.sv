/*
	ECE 437
	By Michael Price

	Hazard Control Unit
*/

// Includes
`include "hazard_unit_if.vh"
`include "cpu_types_pkg.vh"
`include "pipeline_types_pkg.vh"

module hazard_unit (
	hazard_unit_if.hu huif
);

	// Types
	import cpu_types_pkg::*;
	import pipeline_types_pkg::*;

	//logic no_stall;

	always_comb begin

		//no_stall = huif.no_stall;
		
		huif.ifid_flush = huif.flush_ifid || huif.halt;
		huif.idex_flush = huif.flush_idex;
		huif.pc_WEN = huif.pc_change || huif.ihit;

		if (huif.idex_MemRead && ((huif.idex_rt == huif.ifid_rs) || (huif.idex_rt == huif.ifid_rt))) begin
			huif.ifid_stall = 0;
			huif.idex_stall = 1;
			huif.all_stall = 0;	
			huif.no_stall = 0;
		end else if (huif.dhit) begin
			huif.ifid_stall = 1;
			huif.idex_stall = 0;
			huif.all_stall = 0;
			huif.no_stall = 0;
		end else if (huif.mem_access) begin
			huif.ifid_stall = 0;
			huif.idex_stall = 0;
			huif.all_stall = 1;
			huif.no_stall = 0;
		end else if (huif.ihit) begin
			huif.ifid_stall = 0;
			huif.idex_stall = 0;
			huif.all_stall = 0;	
			huif.no_stall = 1;
		end else begin
			huif.ifid_stall = 1;
			huif.idex_stall = 0;
			huif.all_stall = 0;	
			huif.no_stall = 0;
		end

	end

endmodule