/*
  ECE 437
  By Michael Price

  datapath contains register file, control, hazard,
  muxes, and glue logic for processor
*/

// data path interface
`include "datapath_cache_if.vh"

// alu op, mips op, and instruction type
`include "cpu_types_pkg.vh"

module datapath (
  input logic CLK, nRST,
  datapath_cache_if.dp dpif
);
  // Types
  import cpu_types_pkg::*;

  // PC Init
  parameter PC_INIT = 0;

  // Interfaces
  register_file_if rfif();
  alu_if aluif();
  pc_if pcif();
  control_unit_if cuif();
  decode_if dif();
  pipeline_register_if prif();
  hazard_unit_if huif();
  forwarding_unit_if fuif();

  // Mappings
  register_file RF (
    .CLK(CLK),
    .nRST(nRST),
    .rfif(rfif)
  );
  alu ALU (
    .aluif(aluif)
  );
  pc #(.PC_INIT(PC_INIT)) PC (
    .CLK(CLK),
    .nRST(nRST),
    .pcif(pcif)
  );
  control_unit CU (
    .cuif(cuif)
  );
  decode D (
    .dif(dif)
  );
  pipeline_register PR (
    .CLK(CLK),
    .nRST(nRST),
    .prif(prif)
  );
  hazard_unit HU (
    .huif(huif)
  );
  forwarding_unit FU (
    .fuif(fuif)
  );

  // IFID Inputs
  always_comb begin
    prif.ifid_in.imemload = dpif.imemload;
    prif.ifid_in.next_pc = pcif.next_pc;
  end

  // IDEX Inputs
  always_comb begin
    prif.idex_in.opcode = dif.opcode;
    prif.idex_in.rs = dif.rs;
    prif.idex_in.rt = dif.rt;
    prif.idex_in.rd = dif.rd;
    prif.idex_in.imm = dif.imm;
    prif.idex_in.addr = dif.addr;
    prif.idex_in.shamt = dif.shamt;
    prif.idex_in.funct = dif.funct;

    prif.idex_in.Jump = cuif.Jump;
    prif.idex_in.Jal = cuif.Jal;
    prif.idex_in.BranchEq = cuif.BranchEq;
    prif.idex_in.BranchNotEq = cuif.BranchNotEq;
    prif.idex_in.RegToPC = cuif.RegToPC;
    prif.idex_in.ALUOp = cuif.ALUOp;
    prif.idex_in.Imm = cuif.Imm;
    prif.idex_in.SignExtImm = cuif.SignExtImm;
    prif.idex_in.Shamt = cuif.Shamt;
    prif.idex_in.Lui = cuif.Lui;
    prif.idex_in.MemWrite = cuif.MemWrite;
    prif.idex_in.MemRead = cuif.MemRead;
    prif.idex_in.RegWrite = cuif.RegWrite;
    prif.idex_in.RegDst = cuif.RegDst;
    prif.idex_in.Halt = cuif.Halt;

    prif.idex_in.next_pc = prif.ifid_out.next_pc;
    //prif.idex_in.jump_addr = word_t'({prif.ifid_out.next_pc[31:28], dif.addr, 2'b00});
    //prif.idex_in.branch_addr = word_t'(prif.ifid_out.next_pc + {{14{dif.imm[15]}}, dif.imm, 2'b00});

    prif.idex_in.rdat1 = rfif.rdat1;
    prif.idex_in.rdat2 = rfif.rdat2;

    //prif.idex_in.shamt_zero_ext = word_t'({{16{1'b0}}, dif.shamt});
    //prif.idex_in.imm_zero_pad = word_t'({dif.imm, {16{1'b0}}});
    //prif.idex_in.imm_sign_ext = word_t'({{16{dif.imm[15]}}, dif.imm});
    //prif.idex_in.imm_zero_ext = word_t'({{16{1'b0}}, dif.imm});

    prif.idex_in.exmem_forwardA = fuif.exmem_forwardA;
    prif.idex_in.memwb_forwardA = fuif.memwb_forwardA;
    prif.idex_in.exmem_forwardB = fuif.exmem_forwardB;
    prif.idex_in.memwb_forwardB = fuif.memwb_forwardB;
  end

  // EXMEM Inputs
  always_comb begin
    prif.exmem_in.Jump = prif.idex_out.Jump;
    prif.exmem_in.Jal = prif.idex_out.Jal;
    prif.exmem_in.BranchEq = prif.idex_out.BranchEq;
    prif.exmem_in.BranchNotEq = prif.idex_out.BranchNotEq;
    prif.exmem_in.RegToPC = prif.idex_out.RegToPC;
    prif.exmem_in.Lui = prif.idex_out.Lui;
    prif.exmem_in.MemWrite = prif.idex_out.MemWrite;
    prif.exmem_in.MemRead = prif.idex_out.MemRead;
    prif.exmem_in.RegWrite = prif.idex_out.RegWrite;
    prif.exmem_in.Halt = prif.idex_out.Halt;
    prif.exmem_in.RegDst = prif.idex_out.RegDst;

    //prif.exmem_in.imm_zero_pad = prif.idex_out.imm_zero_pad;
    prif.exmem_in.imm = prif.idex_out.imm;

    prif.exmem_in.rdat1 = prif.idex_out.rdat1;
    prif.exmem_in.rdat2 = fuif.forwardB;
    prif.exmem_in.wsel = (prif.idex_out.RegDst) ? prif.idex_out.rd : ((prif.idex_out.Jal) ? 5'd31 : prif.idex_out.rt);
    prif.exmem_in.rt = prif.idex_out.rt;
    prif.exmem_in.rd = prif.idex_out.rd;

    prif.exmem_in.portOut = aluif.portOut;
    prif.exmem_in.zero = aluif.zero;

    //prif.exmem_in.jump_addr = prif.idex_out.jump_addr;
    prif.exmem_in.next_pc = prif.idex_out.next_pc;
    //prif.exmem_in.branch_addr = prif.idex_out.branch_addr;
  end

  // MEMWB Inputs
  always_comb begin
    prif.memwb_in.Jal = prif.exmem_out.Jal;
    prif.memwb_in.Lui = prif.exmem_out.Lui;
    prif.memwb_in.MemRead = prif.exmem_out.MemRead;
    prif.memwb_in.RegWrite = prif.exmem_out.RegWrite;
    prif.memwb_in.Halt = prif.exmem_out.Halt;
    prif.memwb_in.RegDst = prif.exmem_out.RegDst;

    prif.memwb_in.portOut = prif.exmem_out.portOut;

    prif.memwb_in.dmemload = dpif.dmemload;

    prif.memwb_in.next_pc = prif.exmem_out.next_pc;

    //prif.memwb_in.imm_zero_pad = prif.exmem_out.imm_zero_pad;

    prif.memwb_in.wsel = prif.exmem_out.wsel;
    prif.memwb_in.rt = prif.exmem_out.rt;
    prif.memwb_in.rd = prif.exmem_out.rd;
    prif.memwb_in.wdat = fuif.memwb_in_wdat;
  end



  // Decode Logic
  always_comb begin
    dif.instr = prif.ifid_out.imemload;
  end

  // Pipeline Register Logic
  always_comb begin
    prif.ifid_stall = huif.ifid_stall;
    prif.idex_stall = huif.idex_stall;
    prif.all_stall = huif.all_stall;
    prif.no_stall = huif.no_stall;

    prif.ifid_flush = huif.ifid_flush;
    prif.idex_flush = huif.idex_flush;
  end

  // Hazard Logic
  always_comb begin
   huif.halt = prif.idex_out.Halt;

   huif.idex_MemRead = prif.idex_out.MemRead;
   huif.idex_RegWrite = prif.idex_out.RegWrite;
   huif.mem_access = prif.exmem_out.MemRead || prif.exmem_out.MemWrite;

   huif.dhit = dpif.dhit;
   huif.ihit = dpif.ihit;

   huif.idex_rt = prif.idex_out.rt;
   huif.ifid_rs = dif.rs;
   huif.ifid_rt = dif.rt;
  end

  // Forwarding Logic
  always_comb begin
    fuif.ifid_rs = dif.rs;
    fuif.ifid_rt = dif.rt;

    fuif.idex_RegWrite = prif.idex_out.RegWrite;
    fuif.exmem_RegWrite = prif.exmem_out.RegWrite;

    fuif.idex_wsel = (prif.idex_out.RegDst) ? prif.idex_out.rd : ((prif.idex_out.Jal) ? 5'd31 : prif.idex_out.rt);
    fuif.exmem_wsel = prif.exmem_out.wsel;

    fuif.idex_out_rdat1 = prif.idex_out.rdat1;
    fuif.idex_out_rdat2 = prif.idex_out.rdat2;

    fuif.exmem_out_forwardA = prif.idex_out.exmem_forwardA;
    fuif.memwb_out_forwardA = prif.idex_out.memwb_forwardA;
    fuif.exmem_out_forwardB = prif.idex_out.exmem_forwardB;
    fuif.memwb_out_forwardB = prif.idex_out.memwb_forwardB;

    if (prif.exmem_out.Lui) begin
      // TODO: forwardA is not taking corrent exmem_out_wdat
      // Instead seems to be taking portOut even when LUI is asserted
      fuif.exmem_out_wdat = word_t'({prif.exmem_out.imm, {16{1'b0}}});//prif.exmem_out.imm_zero_pad;
    end else if (prif.exmem_out.Jal) begin
      fuif.exmem_out_wdat = prif.exmem_out.next_pc;
    end begin
      fuif.exmem_out_wdat = prif.exmem_out.portOut;
    end

    /*if (prif.memwb_out.MemRead) begin
      fuif.memwb_out_wdat = prif.memwb_out.dmemload;
    end else begin
      fuif.memwb_out_wdat = prif.memwb_out.wdat;
    end*/

  end

  // Datapath Logic
  always_comb begin
    dpif.imemaddr = pcif.current_pc;
    dpif.dmemaddr =  prif.exmem_out.portOut;
    dpif.dmemstore = prif.exmem_out.rdat2;
    dpif.halt = prif.memwb_out.Halt;

    dpif.imemREN = ~prif.memwb_out.Halt;
    dpif.dmemREN = prif.exmem_out.MemRead;
    dpif.dmemWEN = prif.exmem_out.MemWrite;
  end

  // PC Logic
  always_comb begin
    pcif.WEN = huif.pc_WEN;

    huif.flush_ifid = 0;
    huif.flush_idex = 0;
    huif.pc_change = 0;

    if (prif.idex_out.RegToPC) begin
      pcif.new_pc = fuif.forwardA;

      huif.flush_ifid = 1;
      huif.flush_idex = 1;

      huif.pc_change = 1;
    end else if ((prif.idex_out.BranchEq && (fuif.forwardA == fuif.forwardB)) || (prif.idex_out.BranchNotEq && (fuif.forwardA == fuif.forwardB))) begin
      pcif.new_pc = word_t'(prif.idex_out.next_pc + {{14{prif.idex_out.imm[15]}}, prif.idex_out.imm, 2'b00});//prif.exmem_out.branch_addr;

      huif.flush_ifid = 1;
      huif.flush_idex = 1;

      huif.pc_change = 1;
    end else if (dif.opcode === J || dif.opcode === JAL) begin
      pcif.new_pc = word_t'({pcif.next_pc[31:28], dif.addr, 2'b00});//prif.exmem_out.jump_addr;

      //huif.flush_ifid = 1;
      //huif.flush_idex = 1;

      huif.pc_change = 1;
    end else begin
      pcif.new_pc = pcif.next_pc;
    end

    // TODO: Might have to change all this to IDEX for faster jumping
    /*if (prif.exmem_out.RegToPC) begin
      pcif.new_pc = fuif.forwardA;

      huif.flush_ifid = 1;
      huif.flush_idex = 1;

      huif.pc_change = 1;
    end else if ((prif.exmem_out.BranchEq && prif.exmem_out.zero) || (prif.exmem_out.BranchNotEq && ~prif.exmem_out.zero)) begin
      pcif.new_pc = prif.exmem_out.branch_addr;

      huif.flush_ifid = 1;
      huif.flush_idex = 1;

      huif.pc_change = 1;
    end else if (prif.exmem_out.Jump || prif.exmem_out.Jal) begin
      pcif.new_pc = prif.exmem_out.jump_addr;
    end else begin
      pcif.new_pc = pcif.next_pc;
    end*/
  end
  
  // Control Unit Logic
  always_comb begin
    cuif.opcode = dif.opcode;
    cuif.funct = dif.funct;
  end

  // Register File Logic
  always_comb begin
    rfif.WEN = prif.memwb_out.RegWrite;
    rfif.rsel1 = dif.rs;
    rfif.rsel2 = dif.rt;
    rfif.wsel = prif.memwb_out.wsel;
    //rfif.wdat = fuif.wdat;

    if (prif.memwb_out.MemRead) begin
      rfif.wdat = prif.memwb_out.dmemload;
    end else begin
      rfif.wdat = prif.memwb_out.wdat;
    end
  end

  // ALU Logic
  always_comb begin
    aluif.op = prif.idex_out.ALUOp;
    aluif.portA = fuif.forwardA;

    if (prif.idex_out.Shamt) begin
      aluif.portB = word_t'({{16{1'b0}}, prif.idex_out.shamt});//prif.idex_out.shamt_zero_ext; 
    end else if (prif.idex_out.Imm) begin
      if (prif.idex_out.SignExtImm) begin
        aluif.portB = word_t'({{16{prif.idex_out.imm[15]}}, prif.idex_out.imm});//prif.idex_out.imm_sign_ext;
      end else begin
        aluif.portB = word_t'({{16{1'b0}}, prif.idex_out.imm});//prif.idex_out.imm_zero_ext;
      end
    end else begin
      aluif.portB = fuif.forwardB;
    end
  end

endmodule
