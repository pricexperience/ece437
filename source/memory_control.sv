/*
  ECE 437
  By Michael Price

  this block is the coherence protocol
  and artibtration for ram
*/

// interface include
`include "cache_control_if.vh"

// memory types
`include "cpu_types_pkg.vh"

module memory_control (
  input CLK, nRST,
  cache_control_if.cc ccif
);
  // type import
  import cpu_types_pkg::*;

  // number of cpus for cc
  parameter CPUS = 1;
  parameter CPUID = 0;

  always_comb begin

    // Cache outputs
    ccif.iwait[CPUID] = 1'b1;    // Detault state
    ccif.dwait[CPUID] = 1'b1;    // Default state

    ccif.iload[CPUID] = ccif.ramload;
    ccif.dload[CPUID] = ccif.ramload;

    // Ram outputs
    ccif.ramstore = ccif.dstore[CPUID];
    ccif.ramaddr = (ccif.dREN[CPUID] || ccif.dWEN[CPUID]) ? ccif.daddr[CPUID] : ccif.iaddr[CPUID];
    ccif.ramWEN = ccif.dWEN[CPUID];
    ccif.ramREN = ccif.dREN[CPUID] || (ccif.iREN[CPUID] && !ccif.dWEN[CPUID]);

    casez (ccif.ramstate)

      FREE: begin
        ccif.iwait[CPUID] = 1'b0;
        ccif.dwait[CPUID] = 1'b0;
      end

      ACCESS: begin
        if (ccif.dREN[CPUID] == 1'b1 || ccif.dWEN[CPUID]) begin
          ccif.iwait[CPUID] = 1'b1;
          ccif.dwait[CPUID] = 1'b0;
        end else begin
          ccif.iwait[CPUID] = 1'b0;
          ccif.dwait[CPUID] = 1'b1;
        end
      end

    endcase

  end

endmodule
