/*
	ECE 437
	By Michael Price

	Forwarding Unit
*/

`include "forwarding_unit_if.vh"
`include "cpu_types_pkg.vh"

module forwarding_unit (
	forwarding_unit_if.fu fuif
);

	// Types
	import cpu_types_pkg::*;

	always_comb begin

		fuif.exmem_forwardA = (fuif.ifid_rs != 0) && (fuif.idex_RegWrite == 1) && (fuif.ifid_rs == fuif.idex_wsel);
		fuif.memwb_forwardA = (fuif.ifid_rs != 0) && (fuif.exmem_RegWrite == 1) && (fuif.ifid_rs == fuif.exmem_wsel);

		fuif.exmem_forwardB = (fuif.ifid_rt != 0) && (fuif.idex_RegWrite == 1) && (fuif.ifid_rt == fuif.idex_wsel);
		fuif.memwb_forwardB = (fuif.ifid_rt != 0) && (fuif.exmem_RegWrite == 1) && (fuif.ifid_rt == fuif.exmem_wsel);

		fuif.forwardA = (fuif.exmem_out_forwardA) ? fuif.exmem_out_wdat : ((fuif.memwb_out_forwardA) ? fuif.memwb_out_wdat : fuif.idex_out_rdat1);
		fuif.forwardB = (fuif.exmem_out_forwardB) ? fuif.exmem_out_wdat : ((fuif.memwb_out_forwardB) ? fuif.memwb_out_wdat : fuif.idex_out_rdat2);
	
		fuif.memwb_in_wdat = fuif.exmem_out_wdat;
		//fuif.wdat = fuif.memwb_out_wdat;
	end


endmodule