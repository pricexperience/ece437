/*
	ECE 437
	By Michael Price

	Pipeline Register Interface
*/

// Interface definition
`ifndef PIPELINE_REGISTER_IF_VH
`define PIPELINE_REGISTER_IF_VH

// Types
`include "cpu_types_pkg.vh"
`include "pipeline_types_pkg.vh"

interface pipeline_register_if;

	// Types
	import cpu_types_pkg::*;
	import pipeline_types_pkg::*;

	// Hazard Controls
	logic ifid_stall, idex_stall, all_stall, no_stall;
	// Flush Controls
	logic ifid_flush, idex_flush;
	// IFID
	ifid_t ifid_in, ifid_out;
	// IDEX
	idex_t idex_in, idex_out;
	// EXMEM
	exmem_t exmem_in, exmem_out;
	// MEMWB
	memwb_t memwb_in, memwb_out;

	modport pr (
		input ifid_flush, idex_flush,
		input ifid_stall, idex_stall, all_stall, no_stall,
		input ifid_in, idex_in, exmem_in, memwb_in,
		output ifid_out, idex_out, exmem_out, memwb_out
	);

endinterface

`endif



