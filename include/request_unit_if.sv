/*
	ECE437
	Michael Price

	Request Unit Interface
*/

// Interface definition
`ifndef REQUEST_UNIT_IF_VH
`define REQUEST_UNIT_IF_VH

// Types
`include "cpu_types_pkg.vh"

interface request_unit_if;

	import cpu_types_pkg::*;

	// Inputs
	logic MemWrite, MemRead, ihit, dhit, halt;
	// Outputs
	logic dWEN, iREN, dREN;

	// Ports
	module ru (
		input MemWrite, MemRead, ihit, dhit, halt,
		output dWEN, iREN, dREN
	);

endinterface

`endif