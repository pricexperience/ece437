/*
  ECE 437
  By Michael Price

  Control Unit Interface

  Inputs
	opcode
	funct

  Outputs (Control Signals)
  	RegDst
	RegWrite
	ALUSrc
	ALUOp
	MemRead
	MemWrite
	MemToReg

	Branch - use imm
	Jump - use addr
	*JR
	*JAL
	*BNE
*/

// Interface definition
`ifndef CONTROL_UNIT_IF_VH
`define CONTROL_UNIT_IF_VH

// Types
`include "cpu_types_pkg.vh"

interface control_unit_if;

	import cpu_types_pkg::*;

	// Inputs
	opcode_t opcode;
	funct_t funct;
	// Outputs
	aluop_t ALUOp;
	logic RegDst, RegWrite, RegToPC;
	logic MemRead, MemWrite;
	logic BranchEq, BranchNotEq, Jump, Jal;
	logic SignExtImm, Shamt, Imm;
	logic Lui, Halt;

	modport cu (
		input opcode, funct,
		output RegDst, RegWrite, RegToPC,
		output ALUOp,
		output MemRead, MemWrite,
		output BranchEq, BranchNotEq, Jump, Jal,
		output SignExtImm, Shamt, Imm,
		output Lui, Halt
	);

endinterface

`endif