/*
	ECE437
	By Michael Price

	PC interface
*/

// Interface definition
`ifndef PC_IF_VH
`define PC_IF_VH

// Types
`include "cpu_types_pkg.vh"

interface pc_if;

	import cpu_types_pkg::*;

	// Inputs
	word_t new_pc;
	logic WEN;
	// Outputs
	word_t current_pc, next_pc;

	// Ports
	module pc (
		input WEN, new_pc,
		output current_pc, next_pc
	);

endinterface

`endif
