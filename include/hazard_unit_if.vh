/*
	ECE437
	Michael Price

	Hazard Unit Interface
*/

// Interface definition
`ifndef HAZARD_UNIT_IF_VH
`define HAZARD_UNIT_IF_VH

// Types
`include "cpu_types_pkg.vh"

interface hazard_unit_if;

	import cpu_types_pkg::*;

	// Inputs
	logic flush_ifid, flush_idex, pc_change, halt;
	logic idex_MemRead, idex_RegWrite, mem_access;
	logic dhit, ihit;
	regbits_t idex_rt, ifid_rs, ifid_rt;
	// Outputs
	logic ifid_stall, idex_stall, all_stall, no_stall;
	logic ifid_flush, idex_flush;
	logic pc_WEN;

	// Ports
	modport hu (
		input flush_ifid, flush_idex, pc_change, halt,
		input idex_MemRead, idex_RegWrite, mem_access,
		input dhit, ihit, idex_rt, ifid_rs, ifid_rt,
		output ifid_stall, idex_stall, all_stall, no_stall, 
		output ifid_flush, idex_flush, pc_WEN
	);

endinterface

`endif