/*
	ECE 437
	By Michael Price

	Forwarding Unit Interface
*/

// Interface definition
`ifndef FORWARDING_UNIT_IF_VH
`define FORWARDING_UNIT_IF_VH

// Types
`include "cpu_types_pkg.vh"

interface forwarding_unit_if;

	import cpu_types_pkg::*;

	// Inputs
	regbits_t ifid_rs, ifid_rt, idex_wsel, exmem_wsel;
	logic idex_RegWrite, exmem_RegWrite;
	logic exmem_out_forwardA, memwb_out_forwardA;
	logic exmem_out_forwardB, memwb_out_forwardB;
	word_t exmem_out_wdat, memwb_out_wdat;
	word_t idex_out_rdat1, idex_out_rdat2;
	// Outputs
	word_t forwardA, forwardB;
	logic exmem_forwardA, memwb_forwardA;
	logic exmem_forwardB, memwb_forwardB;
	word_t memwb_in_wdat, wdat;

	modport fu (
		input ifid_rs, ifid_rt, idex_wsel, exmem_wsel,
		input idex_RegWrite, exmem_RegWrite, 
		input exmem_out_forwardA, memwb_out_forwardA,
		input exmem_out_forwardB, memwb_out_forwardB,
		input exmem_out_wdat, memwb_out_wdat,
		input idex_out_rdat1, idex_out_rdat2,
		output forwardA, forwardB,
		output exmem_forwardA, memwb_forwardA,
		output exmem_forwardB, memwb_forwardB,
		output memwb_in_wdat, wdat
	); 


endinterface

`endif