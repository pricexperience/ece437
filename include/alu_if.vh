/*

	ECE 437
	ALU Interface
	By Michael Price

*/

`ifndef ALU_IF_VH
`define ALU_IF_VH

`include "cpu_types_pkg.vh"

interface alu_if;

	import cpu_types_pkg::*;

	// Inputs/Outputs
	word_t	portA, portB, portOut;
	aluop_t op;
	logic negative, overflow, zero;	// Flags

	modport alu (
		input portA, portB, op,
		output portOut, negative, overflow, zero
	);

	modport tb (
		input portOut, negative, overflow, zero,
		output portA, portB, op
	);


endinterface

`endif