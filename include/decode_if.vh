/*
  ECE 437
  By Michael Price

  Decode Interface

  Inputs
  	instr

  Outputs
	opcode
	rs
	rt
	rd
	shamt
	funct
	imm
	addr
*/

// Interface definition
`ifndef DECODE_IF
`define DECODE_IF

// Types
`include "cpu_types_pkg.vh"

interface decode_if;

	import cpu_types_pkg::*;

	// Inputs
	word_t instr;
	// Outputs
	opcode_t opcode;
	regbits_t rs, rt, rd;
	logic [SHAM_W-1:0] shamt;
	funct_t funct;
	logic [IMM_W-1:0] imm;
	logic [ADDR_W-1:0] addr;

	modport d (
		input instr,
		output opcode, rs, rt, rd, shamt, funct, imm, addr
	);


endinterface


`endif