/*
	ECE 437
	By Michael Price

	Pipeline register I/O's
*/

// Package definitions
`ifndef PIPELINE_TYPES_PKG
`define PIPELINE_TYPES_PKG

// CPU Types
`include "cpu_types_pkg.vh"

package pipeline_types_pkg;

	// CPU Types
	import cpu_types_pkg::*;

	// IF/ID
	typedef struct packed {

		// icache
		word_t imemload;
		// PC in
		word_t next_pc;

	} ifid_t;

	typedef struct packed {

		// Control signals
		logic Jump;
		logic Jal;
		logic BranchEq;
		logic BranchNotEq;
		logic RegToPC;
		aluop_t ALUOp;
		logic Imm;
		logic SignExtImm;
		logic Shamt;
		logic Lui; 
		logic MemWrite;
		logic MemRead;
		logic RegDst;
		logic RegWrite;
		logic Halt;

		// Register File
		opcode_t opcode;
		regbits_t rs, rt, rd;
		funct_t funct;
		logic [IMM_W-1:0] imm;
		logic [ADDR_W-1:0] addr;
		logic [SHAM_W-1:0] shamt;

		// PC
		word_t next_pc;
		//word_t jump_addr;
		//word_t branch_addr;

		// Register File
		word_t rdat1;
		word_t rdat2;

		// Extension Operations
		//word_t shamt_zero_ext;
		//word_t imm_zero_pad;
		//word_t imm_sign_ext;
		//word_t imm_zero_ext;

		// Forwarding
		logic exmem_forwardA;
		logic memwb_forwardA;
		logic exmem_forwardB;
		logic memwb_forwardB;


	} idex_t;

	typedef struct packed {

		// Control Signals
		logic Jump;
		logic Jal;
		logic BranchEq;
		logic BranchNotEq;
		logic RegToPC;
		logic Lui;
		logic MemWrite;
		logic MemRead;
		logic RegWrite;
		logic Halt;
		logic RegDst; // Inserted

		// Extension Operations
		//word_t imm_zero_pad;

		// Register File
		word_t rdat1;
		word_t rdat2;
		regbits_t wsel;
		regbits_t rt, rd;
		logic [IMM_W-1:0] imm;

		// ALU
		word_t portOut;
		word_t zero;

		// PC
		//word_t jump_addr;
		word_t next_pc;
		//word_t branch_addr;

	} exmem_t;

	typedef struct packed {

		// Control Signals
		logic Jal;
		logic Lui; 
		logic MemRead;
		logic RegWrite;
		logic Halt;
		logic RegDst;

		// ALU
		word_t portOut;

		// dcache
		word_t dmemload;

		// PC
		word_t next_pc;

		// Extension Operations
		//word_t imm_zero_pad;

		// Register File
		regbits_t wsel; 
		regbits_t rt, rd;
		word_t wdat;

	} memwb_t;

endpackage

`endif