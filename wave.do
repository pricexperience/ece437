onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /system_tb/CLK
add wave -noupdate /system_tb/nRST
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[8]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[7]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[6]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[5]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[4]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[3]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[2]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[1]}
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/WEN
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/wsel
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rsel1
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rsel2
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/wdat
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rdat1
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rdat2
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out.imm_zero_pad
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out.portOut
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out.dmemload
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out.next_pc
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out.MemRead
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.Shamt
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.SignExtImm
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.Imm
add wave -noupdate /system_tb/DUT/CPU/DP/prif/exmem_in.portOut
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.shamt_zero_ext
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.rdat1
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.rdat2
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.imm_sign_ext
add wave -noupdate /system_tb/DUT/CPU/DP/prif/idex_out.imm_zero_ext
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {380656 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1024 ns}
