onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /system_tb/CLK
add wave -noupdate /system_tb/nRST
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[7]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[5]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[4]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[3]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[2]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[1]}
add wave -noupdate -childformat {{/system_tb/DUT/CPU/DP/prif/idex_out.next_pc -radix hexadecimal}} -subitemconfig {/system_tb/DUT/CPU/DP/prif/idex_out.next_pc {-height 17 -radix hexadecimal}} /system_tb/DUT/CPU/DP/prif/idex_out
add wave -noupdate /system_tb/DUT/CPU/DP/prif/exmem_out
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out
add wave -noupdate /system_tb/DUT/CPU/DP/cuif/opcode
add wave -noupdate /system_tb/DUT/CPU/DP/cuif/funct
add wave -noupdate /system_tb/DUT/CPU/DP/huif/no_stall
add wave -noupdate /system_tb/DUT/CPU/DP/huif/ifid_stall
add wave -noupdate /system_tb/DUT/CPU/DP/huif/idex_stall
add wave -noupdate /system_tb/DUT/CPU/DP/huif/all_stall
add wave -noupdate /system_tb/DUT/CPU/DP/huif/ifid_flush
add wave -noupdate /system_tb/DUT/CPU/DP/huif/idex_flush
add wave -noupdate /system_tb/DUT/CPU/DP/pcif/WEN
add wave -noupdate /system_tb/DUT/CPU/DP/pcif/current_pc
add wave -noupdate /system_tb/DUT/CPU/DP/pcif/new_pc
add wave -noupdate /system_tb/DUT/CPU/DP/dpif/ihit
add wave -noupdate /system_tb/DUT/CPU/DP/dpif/dhit
add wave -noupdate /system_tb/DUT/CPU/DP/huif/ifid_rt
add wave -noupdate /system_tb/DUT/CPU/DP/huif/ifid_rs
add wave -noupdate /system_tb/DUT/CPU/DP/huif/idex_rt
add wave -noupdate /system_tb/DUT/CPU/DP/huif/idex_MemRead
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {200000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 168
configure wave -valuecolwidth 162
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1018370 ps}
