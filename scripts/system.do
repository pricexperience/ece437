onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /system_tb/CLK
add wave -noupdate /system_tb/nRST
add wave -noupdate /system_tb/DUT/CPU/DP/cuif/opcode
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[15]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[3]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[2]}
add wave -noupdate {/system_tb/DUT/CPU/DP/RF/rfile[1]}
add wave -noupdate -childformat {{/system_tb/DUT/CPU/DP/prif/idex_out.next_pc -radix hexadecimal}} -subitemconfig {/system_tb/DUT/CPU/DP/prif/idex_out.next_pc {-height 17 -radix hexadecimal}} /system_tb/DUT/CPU/DP/prif/idex_out
add wave -noupdate /system_tb/DUT/CPU/DP/prif/exmem_out
add wave -noupdate /system_tb/DUT/CPU/DP/prif/memwb_out
add wave -noupdate -radix decimal /system_tb/DUT/CPU/DP/pcif/next_pc
add wave -noupdate -radix decimal /system_tb/DUT/CPU/DP/pcif/new_pc
add wave -noupdate -radix decimal /system_tb/DUT/CPU/DP/pcif/current_pc
add wave -noupdate /system_tb/DUT/CPU/DP/pcif/WEN
add wave -noupdate /system_tb/DUT/CPU/DP/huif/ihit
add wave -noupdate -childformat {{/system_tb/DUT/CPU/DP/prif/ifid_out.imemload -radix hexadecimal} {/system_tb/DUT/CPU/DP/prif/ifid_out.next_pc -radix decimal}} -expand -subitemconfig {/system_tb/DUT/CPU/DP/prif/ifid_out.imemload {-height 17 -radix hexadecimal} /system_tb/DUT/CPU/DP/prif/ifid_out.next_pc {-height 17 -radix decimal}} /system_tb/DUT/CPU/DP/prif/ifid_out
add wave -noupdate /system_tb/DUT/CPU/DP/huif/pc_change
add wave -noupdate /system_tb/DUT/CPU/dcif/dmemstore
add wave -noupdate /system_tb/DUT/CPU/dcif/dmemaddr
add wave -noupdate /system_tb/DUT/CPU/dcif/dmemWEN
add wave -noupdate /system_tb/DUT/CPU/dcif/dhit
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rdat2
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rdat1
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rsel2
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/rsel1
add wave -noupdate /system_tb/DUT/CPU/DP/dif/shamt
add wave -noupdate -radix unsigned /system_tb/DUT/CPU/DP/dif/rt
add wave -noupdate -radix unsigned /system_tb/DUT/CPU/DP/dif/rs
add wave -noupdate -radix unsigned /system_tb/DUT/CPU/DP/dif/rd
add wave -noupdate /system_tb/DUT/CPU/DP/dif/opcode
add wave -noupdate /system_tb/DUT/CPU/DP/dif/instr
add wave -noupdate /system_tb/DUT/CPU/DP/dif/imm
add wave -noupdate /system_tb/DUT/CPU/DP/dif/funct
add wave -noupdate /system_tb/DUT/CPU/DP/dif/addr
add wave -noupdate /system_tb/DUT/CPU/DP/rfif/wsel
add wave -noupdate /system_tb/DUT/CPU/DP/fuif/forwardB
add wave -noupdate /system_tb/DUT/CPU/DP/fuif/forwardA
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {307536 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 168
configure wave -valuecolwidth 162
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2036752 ps}
